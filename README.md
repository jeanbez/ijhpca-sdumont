# I/O Performance of the Santos Dumont Supercomputer

*Jean Luca Bez (1), André Ramos Carneiro (2), Pablo José Pavan (1), Valéria Soldera Girelli (1), Francieli Zanon Boito (3), Bruno Alves Fagundes (2), Carla Osthoff (2), Pedro Leite da Silva Dias (4), Jean-François Méhaut (3) and Philippe O. A. Navaux (1)*

(1) Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Porto Alegre, Brazil

(2) Laboratory for Scientific Computing (LNCC), Petrópolis, Brazil

(3) Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France

(4) Institute of Astronomy, Geophysics and Atmospheric Sciences, University of São Paulo (USP), São Paulo, Brazil